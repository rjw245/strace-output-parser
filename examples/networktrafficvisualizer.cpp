/**
 * Copy this file over src/main.cpp and compile as usual. (sorry to lazy to extend CMakeLists.txt)
 */
#include <iostream>
#include <sstream>
#include <cstdio>

#include "Parser.H"
#include "Skeleton.H"
#include "Printer.H"

using namespace std;

template <typename T, typename T2>
string to_tcpdump_timestamp(T secs, T2 microsecs) {
    time_t     now = static_cast<time_t>(secs->integer_ + (2 * 60 * 60));;
    struct tm  ts  = *gmtime(&now);
    char       buf[80] = {0x00};
    strftime(buf, sizeof(buf), "%H:%M:%S.", &ts);
    sprintf(buf, "%s%06d", buf, microsecs->integer_);
    return buf;
}

/**
 * Capturing on CentOS7u2 with Linux rb-kerberos2 3.10.0-327.3.1.el7.x86_64 #1 SMP Wed Dec 9 14:09:15 UTC 2015 x86_64 x86_64 x86_64 GNU/Linux
 *
 * the strace output contains unique functions. we are capturing all process, and will focus only on connections that are initiated 
 * in the *outgoing* direction. this means f.i. socket() + connect() , and not accept() etc.
 *
 * some context relevant for our capturing:
 *
 * accept - ignoring
 * bind - ignoring
 * [x] connect - RELEVANT - can refine a socket with information
 * getpeername - ignoring
 * getsockname - ignoring
 * getsockopt - ignoring
 * inet_addr - not a top level function, but we might need this one
 * listen - ignoring
 * msg_iov - not a top level function, afaik only used with sendmmsg.
 * [x] recvfrom - RELEVANT
 * [~] sendfile - RELEVANT
 * [x] sendmmsg - in my captures only used by DNS
 * [x] sendto - RELEVANT
 * setsockopt - ignoring
 * [ ] shutdown - RELEVANT (closes stream)
 * [x] socket - RELEVANT
 * socketpair - ignoring
 *
 * [generated with: cat /tmp/job.strace /tmp/strace-output-parser/*.strace | ./strace-output-parser 2>/dev/null| grep -v "line 1" | sort -u]
 *
 * so this visitor will do the following:
 *
 * - capture streams for 'network' sockets only (TCP sockets that connect() to a specific host+port, and UDP where that info is provided in sendto()/recfrom())
 * - a stream is closed when it's overwritten or explicitly called shutdown()
 * - each of the captured streams output HOST, timestamp, pid, bytes written and bytes read.
 *   (HOST is provided via commandline arguments to this program)
 */
class Stream
{
    string hostname_;
    int64_t process_id_;
    int64_t fd_;

    string dest_;
    int64_t port_;

public:
    Stream(string hostname, int64_t process_id, int64_t fd) 
        : hostname_(hostname), process_id_(process_id), fd_(fd), dest_(), port_(0)
    {}

    string debug()
    {
        stringstream ss;
        ss << "stream=" << hostname_ << " " << process_id_ << " " << fd_ << " && " << dest_ << "," << port_ << endl;
        return ss.str();
    }

    void add_dest(string dest) {
        dest_ = dest;
    }
    void add_port(int64_t port) {
        port_ = port;
    }

    friend bool operator== (const Stream &a, const Stream &b);

    int64_t port() { return port_; }
    string dest() { return dest_; }
    int64_t fd() { return fd_; }

};
bool operator== (const Stream &a, const Stream &b)
{
    return (a.hostname_ == b.hostname_ &&
            a.process_id_ == b.process_id_ &&
            a.fd_ == b.fd_);
}
#include <algorithm>
class Streams
{
    vector<Stream> streams;

public:

    void add(Stream s) {
        streams.push_back(s);
    }
    bool has(Stream s) {
        return std::find(streams.begin(), streams.end(), s) != streams.end();
    }
    Stream &get(Stream s) {
        return *std::find(streams.begin(), streams.end(), s);
    }
    void remove(Stream s) {
        streams.erase(std::remove_if(streams.begin(), streams.end(), [&](Stream &s2) -> bool {
            return s == s2;
        }), streams.end()); 
    }
    void add_dest(Stream s, string destination) {
        if (has(s)) {
            auto &stream = get(s);
            stream.add_dest(destination);
        }
    }
    void add_port(Stream s, int64_t port) {
        if (has(s)) {
            auto &stream = get(s);
            stream.add_port(port);
        }
    }
    void debug() {
        for (auto &stream : streams) {
            cout << "debug = " << stream.debug() << endl;
        }
    }
};

class NetworkTrafficVisitor : public Skeleton
{
    std::string line;
    Streams streams_;
    string timestamp;
    string function;
    string strings;
    string hostname_;
    int64_t process_id;
    int64_t stream;
    int64_t stream2;

    size_t list_param_level_;
    bool first_param_not_int_;
    bool stream_matched_;
    size_t function_num_;

    bool capture_port;
    bool capture_dest;

    string destination;
    int64_t port;

    size_t bytes;
    int capture_sendmmsg;

    size_t  capture_getsockname;

public:
    void set_line_ref(const std::string &line_ref) { line = line_ref; } // debug
    NetworkTrafficVisitor(string hostname) : hostname_(hostname) {}

    void visitEStraceLine(EStraceLine* p)
    {
        timestamp         = "";
        function          = "";
        strings           = "";
        process_id        = 0;
        list_param_level_ = 0;
        stream            = 0;
        function_num_     = 0;
        capture_port      = false;
        capture_dest      = false;
        destination       = "";
        port              = 0;

        bytes = 0;
        capture_sendmmsg = 0;
        capture_getsockname = 0;

        Skeleton::visitEStraceLine(p);

        auto output = [=](std::string direction, Stream &lookup) {
            if (!streams_.has(lookup)) {
                cout << timestamp << " " << hostname_ << " " << process_id << " " << direction << " ? ? " << bytes << " (debug fd = " << lookup.fd() << ", line = " << line << ")" << endl;
            } else {
                auto stream = streams_.get(lookup);
                if (stream.dest() == "")
                    cout << timestamp << " " << hostname_ << " " << process_id << " " << direction << " ? ? " << bytes << " (debug fd = " << lookup.fd() << ", line = " << line << ")" << endl;
                else
                    cout << timestamp << " " << hostname_ << " " << process_id << " " << direction << " " << stream.dest() << " " << stream.port() << " " << bytes << " (debug fd = " << lookup.fd() << ", line = " << line << ")" << endl;
            }
        };

        if (function == "socket") {
            //cout << timestamp << " " << hostname_ << " " << process_id << " " << function << " " << stream << " (new stream)"<< endl;
            auto newstream = Stream(hostname_, process_id, stream);
            streams_.add(newstream);
        }
        if (function == "connect") {
            //cout << timestamp << " " << hostname_ << " " << process_id << " " << function << " " << stream << " (dest=" << destination << ", port=" << port << ")" << endl;
            auto lookup = Stream(hostname_, process_id, stream);
            if (streams_.has(lookup)) {
                streams_.add_dest(lookup, destination);
                streams_.add_port(lookup, port);
            }
        }
        if (function == "sendmmsg") {
            auto lookup = Stream(hostname_, process_id, stream);
            output("out", lookup);
        }
        if (function == "sendto" || function == "write") {
            auto lookup = Stream(hostname_, process_id, stream);
            output("out", lookup);
        }
        if (function == "recvfrom" || function == "read") {
            auto lookup = Stream(hostname_, process_id, stream);
            output("in", lookup);
        }

        if (function == "sendfile") {
            //int64_t process_id = 0; // sendfile() and getsockname() happen at different process id's within hadoop datanodes, so we'll just ignore PID in this case.
            auto lookup = Stream(hostname_, process_id, stream);
            auto lookup2 = Stream(hostname_, process_id, stream2);
            //cout << "sendfile!" << endl;
            //PrintAbsyn printer; string s(printer.print(const_cast<EStraceLine *>(p))); cout << s << endl;
            //cout << "stream 1 = " << stream << " stream2 : " << stream2 << endl;
            if (streams_.has(lookup)) {
                auto stream  = streams_.get(lookup);
                //cout << "found stream 1 = " << stream.debug() << endl;
            } else {
                //cout << "NOT FOUND: " << lookup.debug() << endl;
            }
            //if (streams_.has(lookup2)) {
            //    auto stream2 = streams_.get(lookup2);
            //    cout << "found stream 2 = " << stream2.debug() << endl;
            //} else {
            //    cout << "NOT FOUND" << lookup2.debug() << endl;
            //}
        }
        /*
         this stuff is only interesting for inbound..

        if (function == "getsockname") {
            //int64_t process_id = 0; // sendfile() and getsockname() happen at different process id's within hadoop datanodes, so we'll just ignore PID in this case.
            auto lookup = Stream(hostname_, process_id, stream);
            //cout << "for sockname fetching : " << lookup.debug() << endl;
            if (!streams_.has(lookup)) {
                streams_.add(lookup);
            }
            //cout << " found and adding: " << destination << " " << port << " to: " << lookup.debug() <<  endl;
            streams_.add_dest(lookup, destination);
            streams_.add_port(lookup, port);
        }
        */
    }
    void visitEPidNumber(EPidNumber *p)
    {
        //process_id = static_cast<int64_t>(p->integer_);
        process_id = 0; // too many mismatches w/regards to filedescriptors used etc.

        Skeleton::visitEPidNumber(p);
    }
    void visitEFunction(EFunction* p)
    {
        function_num_++;
        string func = p->ident_;

        if (function_num_ == 1) // top level
            function = func;

        if (function == "connect" && func == "htons")
            capture_port = true;
        if (function == "connect" && func == "inet_pton" || func == "inet_addr")
            capture_dest = true;
        if (function == "sendmmsg") {
            if (func == "msg_iov") {
                capture_sendmmsg = 2;
            }
            else {
                capture_sendmmsg = 1;
            }
        }
        //218807 getsockname(268, {sa_family=AF_INET, sin_port=htons(1004), sin_addr=inet_addr("10.141.0.1")}, [16]) = 0
        /*
         * again, only for the client source (not interested)...!
        if (function == "getsockname") {
            if (func == "htons") {
                capture_getsockname = 2;
            }
        }
        */

        stream_matched_ = (func == "set_tid_address" /*|| func == "clone"*/);
        first_param_not_int_ = false;

        if (function == "sendto" && func == "htons") {
            capture_port = true;
            //PrintAbsyn printer; string s(printer.print(const_cast<EFunction *>(p))); cout << s << "OK?" << endl;
        }
        if (function == "sendto" || function == "connect") {
            if ((func == "inet_pton" || func == "inet_addr"))
                capture_dest = true;
        } else {
            capture_dest = false;
        }
        Skeleton::visitEFunction(p);
    }

    void visitERetvalNumber(ERetvalNumber* p)
    {
        // In case Return value number is of type "RetvalPositiveNumber", it's a positive integer.
        //  hence it might be a stream that was created by the called function.
        auto si = dynamic_cast<EPositiveNumber *>(p->number_);

        if (si && (function == "recvfrom" || function == "sendto" || function == "write" || function == "read")) {
            int fd = si->integer_;
            bytes += fd;
        }

        // In order for us to think it's a created stream we need to be sure we didn't already
        //  conclude the function already works with a stream (i.e. read(3, ..) = 100).
        if (stream_matched_ || !si)
            return Skeleton::visitERetvalNumber(p);
        
        // Also the value should be >= 3, because otherwise it's the already existant 
        //  stdin/stdout/stderr. We already excluded negative values, as those are parsed as
        //  a ESignedIntNegative object.
        int fd = si->integer_;
        if (fd <= 2 /* MAX(stdin, stdout, stderr) */)
            return Skeleton::visitERetvalNumber(p);

        // A simple check if this corresponds to the first encountered function. 
        // Otherwise if we encounter a function as a parameter deeper in the parsetree.
        if (function_num_ == 1) {
            // Generate unique uuid for this "thread" (communication on specific filedescriptor)
            //string uuidstr(generate_uuid());

            if (function == "clone") {
                //return = ((int64_t)fd);
            }
            else {
                stream = static_cast<int64_t>(fd);
            }
            //registerStream((int64_t)fd, uuidstr);
        }

        return Skeleton::visitERetvalNumber(p);
    }

    void visitListParam(ListParam *p)
    {
        list_param_level_++;

        if ((*p).empty() || !dynamic_cast<EParamInteger *>((*p)[0]) || first_param_not_int_) {
            // Set a flag that we couldn't find a stream as the first parameter. 
            // i.e.: "access("/etc/ld.so.nohwcap", F_OK) = -1 ENOENT (No such file or directory)"
            //       does not work on a stream, that pattern seems to be read(stream, ..). 
            //       (side note: we also conclude it doesn't create a stream due to the negative integer)
            first_param_not_int_ = true;
            Skeleton::visitListParam(p);
            list_param_level_--;
            return;
        }

        auto param          = dynamic_cast<EParamInteger *>((*p)[0]);
        auto positiveNumber = static_cast<EPositiveNumber *>(param->number_);

        // A "function" element can be encountered as parameters etc., more levels away in the tree
        //  we are only interested in the highest level (i.e., read(3, ..)/write(3, ..)'s first parameter.)
        if (list_param_level_ == 1) {

            stream = static_cast<int64_t>(positiveNumber->integer_);

            if ((*p).size() > 1 && dynamic_cast<EParamInteger *>((*p)[1])) {
                // try another one..
                auto param2          = dynamic_cast<EParamInteger *>((*p)[1]);
                auto positiveNumber2 = static_cast<EPositiveNumber *>(param2->number_);
                stream2 = static_cast<int64_t>(positiveNumber2->integer_);
            }

            // Prevent this value from being overwritten by the return value.
            // i.o.w.: "read(3, ...) = 100", the 100 should not be mistaken for a created stream.
            //      in "open(...)    = 3" the "return" *is* the created stream.
            stream_matched_ = true;
        }

        Skeleton::visitListParam(p);

        list_param_level_--;
    }


    void visitEEpochElapsedTime(EEpochElapsedTime *p)
    {
        auto secs      = static_cast<ESeconds *>(p->seconds_);
        auto microsecs = static_cast<EMicroseconds *>(p->microseconds_);
        timestamp = to_tcpdump_timestamp(secs, microsecs);
        Skeleton::visitEEpochElapsedTime(p);
    }

    void visitString(String x)
    {
        if (capture_dest) {
            destination.assign(x);
            capture_dest = false;
            auto lookup = Stream(hostname_, process_id, stream);
            //cout << "looking up: " << hostname_ << " " << process_id << " " << stream << endl;
            if (streams_.has(lookup)) {
                //cout << "captured host = " << destination << endl;
                streams_.add_dest(lookup, destination);
            }
        }
        /* idem.
        if (capture_getsockname > 0) {
            //cout << "capturing getsockname string : " << x << "\n";
            destination.assign(x);
            capture_getsockname--;
        }
        */
        strings.append(x);
        Skeleton::visitString(x);
    }

    void visitInteger(Integer x)
    {
        if (capture_port) {
            port = x;
            // just update inline
            auto lookup = Stream(hostname_, process_id, stream);
            //cout << "looking up: " << hostname_ << " " << process_id << " " << stream << endl;
            if (streams_.has(lookup)) {
                //cout << "captured port = " << port << endl;
                streams_.add_port(lookup, port);
            }
            capture_port = false;
        }
        /*
        if (capture_getsockname == 2) {
            port = x;
            capture_getsockname = 1;
        }
        */
        if (capture_sendmmsg >= 2) capture_sendmmsg++;
        if (capture_sendmmsg == 2 + 2) {
            capture_sendmmsg = 1;
            bytes += x;
        }
        Skeleton::visitInteger(x);
    }
};

int main(int argc, char ** argv)
{
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " <HOSTNAME>" << endl;
        return 1;
    }

    string line;
    NetworkTrafficVisitor parser(argv[1]);
    while (getline(cin, line)) {
        StraceLines *parse_tree = pStraceLines(line.c_str());
        if (!parse_tree) {
            //cout << "err = " << line << endl;
            continue;
        } else {
            //cout << "lin = " << line << endl;
        }
        parser.set_line_ref(line);
        parse_tree->accept(&parser);
    }
    return 0;
}
