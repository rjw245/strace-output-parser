/**
 * Copy this file over src/main.cpp and compile as usual. (sorry to lazy to extend CMakeLists.txt)
 */
#include <iostream>
#include <sstream>
#include <cstdio>

#include "Parser.H"
#include "Skeleton.H"
#include "Printer.H"

using namespace std;

template <typename T, typename T2>
string to_elasticsearch_timestamp(T secs, T2 microsecs) {
    time_t     now = static_cast<time_t>(secs->integer_);;
    struct tm  ts  = *gmtime(&now);
    char       buf[80] = {0x00};
    strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%S.", &ts);
    sprintf(buf, "%s%03.0fZ", buf, static_cast<double>(microsecs->integer_) / 1000.0);
    return buf;
}

class OutputOpenVisitor : public Skeleton
{
    string timestamp;
    string function;
    string strings;
public:
    void visitEStraceLine(EStraceLine* p)
    {
        timestamp = "";
        function  = "";
        strings   = "";
        Skeleton::visitEStraceLine(p);
        if (function == "open") {
            cout << timestamp << " " << function << " " << strings << endl;
        }
    }
    void visitEFunction(EFunction* p)
    {
        function = p->ident_;
        Skeleton::visitEFunction(p);
    }
    void visitEEpochElapsedTime(EEpochElapsedTime *p)
    {
        auto secs      = static_cast<ESeconds *>(p->seconds_);
        auto microsecs = static_cast<EMicroseconds *>(p->microseconds_);
        timestamp = to_elasticsearch_timestamp(secs, microsecs);
        Skeleton::visitEEpochElapsedTime(p);
    }
    void visitString(String x)
    {
        strings.append(x);
        Skeleton::visitString(x);
    }
};

int main(int argc, char ** argv)
{
    string line;
    while (getline(cin, line)) {
        StraceLines *parse_tree = pStraceLines(line.c_str());
        if (!parse_tree) {
            continue;
        }
        OutputOpenVisitor parser;
        parse_tree->accept(&parser);
    }
    return 0;
}
