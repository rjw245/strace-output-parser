# Example usage

My example program `src/main.cpp` when compiled and ran with some test data `test-data/test3.txt` and `elasticsearch` as an argument.

Note that the testdata is generated from the command `strace -fF -ttt -o out.txt -s 80 curl http://cppse.nl/test.txt`.

    trigen@Rays-MacBook-Pro.local:/projects/bnfc[master]> cat test\-data/test3.txt | ./strace\-output\-parser elasticsearch
    [DELETE strace_index from ElasticSearch]
     <<< {"acknowledged":true}

    [PUT strace_index in ElasticSearch]
     <<< {"acknowledged":true}

    [parsing line from stdin]
     line = 15757 1429444463.429679 execve("/usr/bin/curl", ["curl", "http://cppse.nl/test.txt"], [/* 22 vars */]) = 0
     parse_ok  = TRUE
     ast  = (EStraceLines [(EStraceLine [(EPidOutput [(EPidNumber 15757)] )] [(ETimestamp [(EEpochElapsedTime [(ESeconds 1429444463)] [(EMicroseconds 429679)])])] [(ELine [(EFunction "execve")] [(EParams [(EParamString "/usr/bin/curl"), (EParamArray [(EParamString "curl"), (EParamString "http://cppse.nl/test.txt")] ), (EParamArray [(EParamComment [(ECSInteger 22), (ECSIdent "vars")] )] )])] ESpace ESpace [(ERetvalNumber [(EPositiveNumber 0)])] [])])])

    [POST strace_line to strace_index in ElasticSearch]
     >>> {"@timestamp":"2015-04-19T11:54:23.430Z","_id":"1429444463.429679","occured_during_thread_id":"5f1e1611-6b8e-49d8-9c60-89bce44598c1","parse_ok":true,"raw_line":"15757 1429444463.429679 execve(\"\/usr\/bin\/curl\", [\"curl\", \"http:\/\/cppse.nl\/test.txt\"], [\/* 22 vars *\/]) = 0","strings":["\/usr\/bin\/curl","curl","http:\/\/cppse.nl\/test.txt"],"timestamp":1429444463.4296789}
     <<< {"_index":"strace_index","_type":"strace_line","_id":"1429444463.429679","_version":1,"created":true}
    
    ...
    
    [parsing line from stdin]
     line = 15757 1429444463.746415 getpeername(3, {sa_family=AF_INET, sin_port=htons(80), sin_addr=inet_addr("188.226.154.212")}, [16]) = 0
     parse_ok  = TRUE
     ast  = (EStraceLines [(EStraceLine [(EPidOutput [(EPidNumber 15757)] )] [(ETimestamp [(EEpochElapsedTime [(ESeconds 1429444463)] [(EMicroseconds 746415)])])] [(ELine [(EFunction "getpeername")] [(EParams [(EParamInteger [(EPositiveNumber 3)]), (EParamObject [(EParamKeyValue (EParamIdent "sa_family") (EParamIdent "AF_INET")), (EParamKeyValue (EParamIdent "sin_port") (EParamFunction [(EFunction "htons")] [(EParamInteger [(EPositiveNumber 80)])] )), (EParamKeyValue (EParamIdent "sin_addr") (EParamFunction [(EFunction "inet_addr")] [(EParamString "188.226.154.212")] ))] ), (EParamArray [(EParamInteger [(EPositiveNumber 16)])] )])] ESpace ESpace [(ERetvalNumber [(EPositiveNumber 0)])] [])])])
    [POST strace_line to strace_index in ElasticSearch]
     >>> {"@timestamp":"2015-04-19T11:54:23.746Z","_id":"1429444463.746415","extracted_kv_pairs":{"sa_family":"AF_INET","sin_addr":"inet_addr (\"188.226.154.212\")","sin_port":"htons (80)"},"occured_during_thread_id":"a47a3d8c-8014-4d76-aaa6-81ed539f8c2b","parse_ok":true,"raw_line":"15757 1429444463.746415 getpeername(3, {sa_family=AF_INET, sin_port=htons(80), sin_addr=inet_addr(\"188.226.154.212\")}, [16]) = 0","stream":3,"stream_state_uses":true,"strings":["188.226.154.212"],"thread_id":"a47a3d8c-8014-4d76-aaa6-81ed539f8c2b","timestamp":1429444463.7464149}
     <<< {"_index":"strace_index","_type":"strace_line","_id":"1429444463.746415","_version":1,"created":true}

The BNF that was used for the parser is found in `bnf/strace-output.cf`.

Given the following example input for `StraceLines`:

    15757 1429444463.750111 poll([{fd=3, events=POLLIN|POLLPRI|POLLRDNORM|POLLRDBAND}], 1, 0) = 1 ([{fd=3, revents=POLLIN|POLLRDNORM}])

The example is parsed into the following Abstract Syntax / Linearized Tree:

    [Abstract Syntax]
    
    (EStraceLines [
        (EStraceLine 
            [(EPidOutput [(EPidNumber 15757)])] 
            [(ETimestamp [(EEpochElapsedTime
                             [(ESeconds 1429444463)]
                             [(EMicroseconds 750111)])])] 
            [(ELine 
                [(EFunction "poll")] 
                [(EParams [
                    (EParamArray [
                        (EParamObject [
                            (EParamKeyValue (EParamIdent "fd") 
                                            (EParamInteger [(EPositiveNumber 3)])),
                            (EParamKeyValue (EParamIdent "events")
                                            (EParamFlags [
                                                (EFlag "POLLIN"),
                                                (EFlag "POLLPRI"),
                                                (EFlag "POLLRDNORM"),
                                                (EFlag "POLLRDBAND")]))])]), 
                    (EParamInteger [(EPositiveNumber 1)]),
                    (EParamInteger [(EPositiveNumber 0)])])]
    
                ESpace ESpace 
    
                [(ERetvalNumber [(EPositiveNumber 1)])] 
    
                [(ETrailingDataParams 
                    [(EParamArray 
                        [(EParamObject [
                            (EParamKeyValue (EParamIdent "fd")
                                            (EParamInteger [(EPositiveNumber 3)])),
                            (EParamKeyValue (EParamIdent "revents")
                                            (EParamFlags [
                                                (EFlag "POLLIN"),
                                                (EFlag "POLLRDNORM")]))])])])
                ]
              )
            ]
         )
    ])
    
    [Linearized Tree]
    15757  1429444463. 750111  poll ([
    {
          fd = 3events = POLLIN | POLLPRI | POLLRDNORM | POLLRDBAND}
          ] 10)   =   1 ( [
          {
                fd = 3revents = POLLIN | POLLRDNORM}
                ])

What the `elasticsearch` program does is:

- Create a strace_index in localhost:9200 ElasticSearch.
- Parse strace output line from stdin.
- Use the MetaDataParser visitor to extract some meta data in Json format.
- Synchronize the Json payload to ElasticSearch.

The data should now be usable with Kibana or some other analysis Frontend.

# Building

    make

If needed the Makefile that is provided can be regenerated for your specific
platform with cmake:

    cmake .

# Re-building the parser code

You will need bnfc, which can be obtained via http://bnfc.digitalgrammars.com/.

I've made it so that if you remove the file `src/Skeleton.C` the regeneration
by `bnfc` will occur after `make`.

Alternatives:

    make -j 8         # build with 8 threads
    make VERBOSE=true # build with debug output

# Some notes for Ubuntu

Requirements install probably:

    sudo apt-get install libcurl4-openssl-dev
    sudo apt-get install libboost-dev
