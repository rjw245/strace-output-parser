#ifndef SKELETON_HEADER
#define SKELETON_HEADER
/* You might want to change the above name. */

#include "Absyn.H"


class Skeleton : public Visitor
{
public:
  void visitStraceLines(StraceLines* p);
  void visitStraceLine(StraceLine* p);
  void visitPid(Pid* p);
  void visitPidNumber(PidNumber* p);
  void visitTimestamp(Timestamp* p);
  void visitLine(Line* p);
  void visitFunction(Function* p);
  void visitParams(Params* p);
  void visitParam(Param* p);
  void visitDateYear(DateYear* p);
  void visitDateMonth(DateMonth* p);
  void visitDateDay(DateDay* p);
  void visitTimeHour(TimeHour* p);
  void visitTimeMinute(TimeMinute* p);
  void visitTimeSecond(TimeSecond* p);
  void visitOperator(Operator* p);
  void visitEpochElapsedTime(EpochElapsedTime* p);
  void visitSeconds(Seconds* p);
  void visitMicroseconds(Microseconds* p);
  void visitCommentString(CommentString* p);
  void visitNumber(Number* p);
  void visitFlag(Flag* p);
  void visitReturnValue(ReturnValue* p);
  void visitAddress(Address* p);
  void visitTrailingData(TrailingData* p);
  void visitWhitespace(Whitespace* p);
  void visitEStraceLines(EStraceLines* p);
  void visitEStraceLine(EStraceLine* p);
  void visitEPidStdOut(EPidStdOut* p);
  void visitEPidOutput(EPidOutput* p);
  void visitEPidNumber(EPidNumber* p);
  void visitETimestamp(ETimestamp* p);
  void visitELine(ELine* p);
  void visitELineUnfinished(ELineUnfinished* p);
  void visitELineContinued(ELineContinued* p);
  void visitELineExited(ELineExited* p);
  void visitEFunction(EFunction* p);
  void visitEFunctionPrivate(EFunctionPrivate* p);
  void visitEParams(EParams* p);
  void visitEParamArray(EParamArray* p);
  void visitEParamObject(EParamObject* p);
  void visitEParamComment(EParamComment* p);
  void visitEParamInteger(EParamInteger* p);
  void visitEParamFlags(EParamFlags* p);
  void visitEParamIdent(EParamIdent* p);
  void visitEParamString(EParamString* p);
  void visitEParamWhitespace(EParamWhitespace* p);
  void visitEParamAddress(EParamAddress* p);
  void visitEParamDateTime(EParamDateTime* p);
  void visitEParamKeyValue(EParamKeyValue* p);
  void visitEParamKeyValueCont(EParamKeyValueCont* p);
  void visitEParamExpression(EParamExpression* p);
  void visitEParamFunction(EParamFunction* p);
  void visitEDateYear(EDateYear* p);
  void visitEDateMonth(EDateMonth* p);
  void visitEDateDay(EDateDay* p);
  void visitETimeHour(ETimeHour* p);
  void visitETimeMinute(ETimeMinute* p);
  void visitETimeSecond(ETimeSecond* p);
  void visitEOperatorMul(EOperatorMul* p);
  void visitEOperatorAdd(EOperatorAdd* p);
  void visitEEpochElapsedTime(EEpochElapsedTime* p);
  void visitESeconds(ESeconds* p);
  void visitEMicroseconds(EMicroseconds* p);
  void visitECSString(ECSString* p);
  void visitECSIdent(ECSIdent* p);
  void visitECSInteger(ECSInteger* p);
  void visitENegativeNumber(ENegativeNumber* p);
  void visitEPositiveNumber(EPositiveNumber* p);
  void visitEFlag(EFlag* p);
  void visitEFlagUmask(EFlagUmask* p);
  void visitERetvalAddress(ERetvalAddress* p);
  void visitERetvalNumber(ERetvalNumber* p);
  void visitERetvalUnknown(ERetvalUnknown* p);
  void visitEAddress(EAddress* p);
  void visitETrailingDataConst(ETrailingDataConst* p);
  void visitETrailingDataParams(ETrailingDataParams* p);
  void visitESpace(ESpace* p);
  void visitESpace4x(ESpace4x* p);
  void visitETab(ETab* p);
  void visitListCommentString(ListCommentString* p);
  void visitListParam(ListParam* p);
  void visitListPid(ListPid* p);
  void visitListTimestamp(ListTimestamp* p);
  void visitListTrailingData(ListTrailingData* p);
  void visitListWhitespace(ListWhitespace* p);
  void visitListFlag(ListFlag* p);
  void visitListStraceLine(ListStraceLine* p);

  void visitHexChar(HexChar x);
  void visitInteger(Integer x);
  void visitChar(Char x);
  void visitDouble(Double x);
  void visitString(String x);
  void visitIdent(Ident x);

};


#endif
